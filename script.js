const registerForm = document.querySelector("#registerForm")

registerForm.addEventListener("submit", (e) => {
	e.preventDefault()

	const username = document.querySelector("#username").value
	const email = document.querySelector("#email").value
	const mobileNo = document.querySelector("#mobileNo").value
	const password1 = document.querySelector("#password1").value
	const password2 = document.querySelector("#password2").value
	//console.log(password1.length)

	let condition1 = false
	let condition2 = false
	let condition3 = false // 1. uppercase

	if(password1.length <= 5){
		alert("PASSWORD MUST BE AT LEAST 6 CHARACTERS LONG")
	}else{
		condition1 = true
	}


	if(password1.search(/[A-Z]/) < 0) {
	    alert("PASSWORD MUST INCLUDE AT LEAST ONE UPPERCASE CHARACTER")
	}else{
		condition2 = true
	}

	if(password1 !== password2){
		alert("PASSWORDS MUST MATCH")
	}else{
		condition3 = true
	}

	//// 2. should be && not $$
	if(condition1 === true && condition2 === true && condition3 === true){
		alert(`USER ${username} REGISTERED WITH EMAIL ${email} AND MOBILE NUMBER ${mobileNo}`)
		username == ""
		email ==  ""
		mobileNo == ""
		password1 == ""
		password2 == ""
	}
})
